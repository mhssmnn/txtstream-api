<?php

/**
 * Txtstream API interface for sending SMS messages.
 *
 * Interface to the TSAPI HTTP service for http://www.txtstream.co.nz/
 * See: http://www.txtstream.co.nz/Integration.html
 * and: http://www.txtstream.co.nz/download/tsapi.doc
 * 
 * @author Luke Hudson <lukeletters@gmail.com>
 * 
 * This class can be used to send text messages (SMS).
 * 
 * Basic Usage:
 * 
 *		$api = new TxtstreamAPI('apiusername', 'apipass');
 *		$response = $api->sendSMS('+64212106318', 'This is a test message');
 *		if ($response->isSuccess()) {
 *			...
 *		} else {
 *			print_r($response->getErrors());
 *		}
 */
class TxtstreamAPI {

	// Hard-coded service URL.
	private static $service_url = 'https://sec.txtstream.co.nz/tsapi.asp';

	/**
	 * TSAPI Service auth credentials.
	 */
	protected static $username = '';
	protected static $password = '';

	/**
	 * Name for message sender.
	 * TODO: How is this used by TSAPI?
	 */
	protected static $from_name = '';
	/**
	 * Email address for reply...
	 * TODO: Work out how this is used exactly by TSAPI.
	 */
	protected static $reply_to = '';

	/**
	 * Setters for API credentials.
	 */
	public function set_username($name) { self::$username = $name; }
	public function set_password($pass) { self::$password = $pass; }

	/**
	 * Set up the 'from' details, in case you want to send several messages with
	 * the same 'from', without having to pass it in sendSMS each time.
	 * @param string $name
	 * @param string $replyTo 
	 */
	public function set_from_details($name, $replyTo) {
		self::$from_name = $name;
		self::$reply_to = $replyTo;
	}

	/**
	 * Construct this instance, providing API access credentials.
	 * @param string $username
	 * @param string $pass
	 * @param array $options unused
	 */
	public function __construct($username = null, $pass = null, array $options = array()) {
		if (!empty($username)) {
			self::set_username($username);
		}
		if (!empty($pass)) {
			self::set_password($pass);
		}
	}

	/**
	 * Send a text message.
	 * @param string $to phone number with international prefix etc.
	 * // TODO: What is to[name] for?
	 * @param string $message text content of SMS
	 * @param string $fromName optional name of sender
	 * @param string $replyTo optional email address where replies should be sent
	 *  // TODO:wtf is the above, actually?
	 * @param string $messageID optional desired messageID.  Note this is not 
	 *  guaranteed to be used. @see Txtstream_SendResponse
	 * @return Txtstream_Response
	 * @throws Txtstream_NetworkException in case of network errors.
	 * @throws Txtstream_ParameterException in case of invalid parameters.
	 */
	public function sendSMS($to, $message, $fromName = null, $replyTo = null, $messageID = null) {
		if (empty($fromName)) {
			$fromName = self::$from_name;
		}
		if (empty($replyTo)) {
			$replyTo = self::$reply_to;
		}
		$msgIDElt = '';
		if (!empty($messageID)) {
			$msgIDElt = '<messageid>' . $messageID . '</messageid>';
		}
		$username = self::$username;
		$password = self::$password;
		$toNumber = $toName = '';
		if (is_array($to)) {
			if (!(isset($to['number']) && isset($to['name']))) {
				throw new Txtstream_ParameterException(
					'Invalid $to array passed to TxtstreamAPI::sendSMS,'
				  . ' should contain elements "number" and "name"'
			  );
			}
			$toNumber = $to['number'];
			$toName = $to['name'];
		} else {
			$toNumber = $to;
			$toName = 'Unknown';
		}
		$xml = include('sendsms_xml_tpl.php');
		$resp = $this->request($xml);
		if (is_string($resp)) {
			try {
				//NOTE: using error-suppression here, as we'll use the 
				//exceptions instead. Don't care about warnings if the XML gets 
				//parsed in the end.
				$resp = new SimpleXMLElement($resp);
			} catch(Exception $ex) {
				throw new Txtstream_NetworkException("Invalid network response: invalid XML", 0, $ex);
			}
			// TODO: Handle multiple results in response (only if multiple requests were sent)
			return new Txtstream_SendResponse($resp);
		}		
		if (!$resp instanceof HTTP_Request2_Response) {
			throw new Txtstream_NetworkException("Unknown network error, invalid response object received from TxtstreamAPI::request");
		}		
		return new Txtstream_ErrorResponse(sprintf("HTTP %d - %s", $resp->getStatus(), $resp->getReasonPhrase()));
	}

	/**
	 * Find out a message's status in Txtstream.
	 * 
	 * @param string $messageID  Upstream identifier for the message (see Txtstream_SendResponse)
	 * @return Txtstream_Response
	 * @throws Txtstream_NetworkException in case of network errors.
	 * @throws Txtstream_ParameterException in case of invalid parameters.
	 */
	public function getMessageStatus($messageID) {
		if (empty($messageID)) {
			throw new Txtstream_ParameterException('Empty $messageID passed to TxtstreamAPI::getMessageStatus');
		}
		$username = self::$username;
		$password = self::$password;
		$xml = include('getstatus_xml_tpl.php');
		$resp = $this->request($xml);
		if (is_string($resp)) {
			try {
				//NOTE: using error-suppression here, as we'll use the 
				//exceptions instead. Don't care about warnings if the XML gets 
				//parsed in the end.
				@$resp = new SimpleXMLElement($resp);
			} catch(Exception $ex) {
				throw new Txtstream_NetworkException("Invalid network response, not XML",0, $ex);
			}
			// TODO: Handle multiple results in response (if multiple requests sent)
			return new Txtstream_StatusResponse($resp);
		}
		if (!$resp instanceof HTTP_Request2_Response) {
			throw new Txtstream_NetworkException("Unknown network error, invalid response object received from TxtstreamAPI::request");
		}		
		return new Txtstream_ErrorResponse(sprintf("HTTP %d - %s", $resp->getStatus(), $resp->getReasonPhrase()));
	}

	/**
	 * Make the HTTP request to the Txtstream API
	 * 
	 * @param string $xml Request XML
	 * @return mixed - on success, the XML response text is returned. Or if
	 *  the request does not return 200/OK, then the HTTP_Response2 object is
	 *  returned for inspection instead.
	 * @throws Txtstream_NetworkException in case of network errors.
	 */
	protected function request($xml) {
		require_once 'HTTP/Request2.php'; // PEAR

		$request = new HTTP_Request2(self::$service_url, HTTP_Request2::METHOD_POST, array('ssl_verify_peer' => false));
		$request->setBody($xml);
		try {
			$response = $request->send();
			if (200 == $response->getStatus()) {
				return $response->getBody();
			} else {
				return $response;
			}
		} catch (HTTP_Request2_Exception $e) {
			// The exception is stored this so it can be checked.  Not really 
			// the way to handle exceptions, but currently the app not well set 
			// up to deal with ex-handling. TODO: Improve this?
			throw new Txtstream_NetworkException(sprintf("Network error in TxtstreamAPI::request: %s", $e->getMessage()), 0, $e);
		}
	}

}


/**
 * Base class for all responses based on receipt of valid XML information
 * @see TxtstreamAPI::request
 * This class contains elements which are common to all Txtstream responses,
 * and methods to deal with those elements.
 */
class Txtstream_Response {
	/**
	 * Error codes returnable from Txtstream API
	 * These will be in the 'code' field of entries in the array $this->errors.
	 */
	const ERR_GNOXML = -1;		//	There was no xml in the body
	const ERR_GBADXML = -2; 	//	There was an error in the form of the xml. <info> will pinpoint what
	const ERR_GBADAUTH = -3;	//	The account and/or password are wrong
	const ERR_GBADREQUEST = -4;	//	Invalid or missing request type

	const ERR_MBADNUMBER = -5;	//	Mobile, the mobile number is bad, see <info> for details
	const ERR_MNOMSG = -6;		//	MessageText, No message text has been supplied and no globalmessagetext supplied
	const ERR_MTRANSIT = -7;	//	TransmitStatus, there was an error submitting the request for subsequent transmission to the SMSC, see <info> for details
	const ERR_MBADREPLYTO = -8;	//	The reply email address was supplied but is badly formed

	/**
	 * All Response objects have an errors array, which will either be empty, or
	 * else will contain structure as follows:
	 * 
	 * array( 
	 *	array('type'	=> '', 'code' => -2, 'info'	=> "Invalid account"),
	 *   . . .
	 * );
	 * 
	 * @var type array
	 */
	public $errors = array();

	/**
	 *
	 * @var type 
	 */
	public $responseTime;

	/**
	 * The ID which Txtstream uses to refer to this message. It may not be the ID we requested via TxtstreamAPI::sendSMS
	 * @var type string
	 */
	public $messageID;
	
	/**
	 * Mobile number that the message was sent to.
	 * @var type string
	 */
	public $mobile;
	/**
	 * Fully formatted mobile number with intl prefix codes.
	 * @var type string
	 */
	public $intlmobile;
	
	/**
	 *
	 * @var type 
	 */
	public $status;

	protected $header;
	protected $sxml;

	/**
	 * Construct this object and fill in basic data.
	 * @param SimpleXMLElement $sxml Input XML structure from Txtstream API response.
	 */
	public function __construct(SimpleXMLElement $sxml) {
		$this->sxml = $sxml;
		$this->getErrors();		
		$this->responseTime	= $this->getHeader('responsetime');
	}

	/**
	 * Fetch the value of a header field from the response.
	 * @param type $field name of field to fetch, or null to fetch header element
	 * @return type mixed string value of field, or header SimpleXMLElement.
	 */
	protected function getHeader($field = null) {
		$this->header = $this->sxml->header;
		if ($field !== null) {
			return (string) $this->header->$field;
		}
		return $this->header;
	}

	/**
	 * Fill in $this->errors from any errors found in the input XML response.
	 * @param type $errors optional SimpleXMLElement to use as source of error 
	 *  information
	 * @return type array
	 */
	protected function getErrors($errors = null) {
		if (!isset($errors)) {
			$errors = $this->sxml->xpath('//error');
		}
		$this->errors = array();
		if (isset($errors)) {
			foreach($errors as $err) {
				$this->errors[] = array(
					'type'	=> (string) $err->errortype,
					'code'	=> (string) $err->code,
					'info'	=> (string) $err->info
					);
			}
		}
		return $this->errors;
	}

	/**
	 * Assume we succeeded if no errors are found.
	 * @return type boolean
	 */
	public function isSuccess() {
		return empty($this->errors);
	}
}

/**
 * This class is only returned to callers if there was no valid XML returned by
 * the Txtstream service, suggesting network errors.
 */
class Txtstream_ErrorResponse extends Txtstream_Response {

	public function __construct($errorMessage = null) {
		if ($errorMessage === null) {
			$errorMessage = 'An unknown error occurred';
		}
		// fake an error in normal format
		$this->errors = array( array('info' => $errorMessage, 'code' => null, 'type' => null) );		
	}

	/**
	 * Does this response contain valid information, or did an error occur?
	 * @return type boolean
	 */
	public function isSuccess() {
		return false;
	}	
}


/**
 * This class is returned from TxtStreamAPI::sendSMS, and should return us the
 * ID used by the API to refer to the message we just tried to send,
 * as well as basic status information about the send request.  To discover the
 * actual state of the message (e.g. delivered etc.) we need to wait >= 6
 * minutes and use TxtstreamAPI::getMessageStatus()
 */
class Txtstream_SendResponse extends Txtstream_Response {
	public $requestedID;

	public function __construct(SimpleXMLElement $sxml) {
		parent::__construct($sxml);

		$request = $sxml->xpath('/response/messagerequestresults/messagerequest');
		if (count($request)) {
			$request = $request[0];
			$this->requestedID 	= (string) $request->requestedid;
			$this->messageID 	= (string) $request->messageid;
			$this->mobile 		= (string) $request->mobile;
			$this->intlmobile 	= (string) $request->internationalmobile;
			$this->status 		= (string) $request->transmitstatus;
		}

	}

	public function isSuccess() {
		return $this->status == 'OK' && empty($this->errors);
	}
}

/**
 * This class encapsulates a TxtStream response to a status request.
 * This will tell us about delivery status of the message, and pass back the 
 * last reply that may have been made by the handset, if any.
 * 
 * @see TxtStreamAPI::getMessageStatus
 */
class Txtstream_StatusResponse extends Txtstream_Response {
	const STATUS_RECEIVED = 'RCV'; // Received by handset
	const STATUS_PROCESSING = 'SNT'; // Received by message centre, no further data known as yet
	const STATUS_REPLY = 'REPLY'; // Message received and replied to
	const STATUS_UNREACHABLE = 'NUR'; // Unreadchable number -- invalid or dead phone number.
	const STATUS_UNKN_MSG = 'Unknown'; // This message ID is not known to status centre.
	const STATUS_RETRYING = 'Error-retry'; // Retrying delivery to message centre.

	public $laststatusupdate;
	protected $reply;

	public function __construct(SimpleXMLElement $sxml) {
		parent::__construct($sxml);

		$request = $sxml->xpath('/response/statusrequest/messagestatus');
		if (count($request)) {
			$request = $request[0];
			$this->intlmobile 	= $this->mobile = (string) $request->mobile;
			$this->messageID 		= (string) $request->id;
			$this->status 			= (string) $request->status;
			$this->laststatusupdate = (string) $request->laststatusupdate;
			$this->reply 			= (string) $request->reply;
			$this->retries 			= (string) $request->retries;
		}
	}

	/**
	 * Fetch the reply which was sent to the given message, if any.
	 * @return string reply text,
	 *  or null if no reply was given in the status 
	 *  response.
	 */
	public function getReply() {
		if ($this->status == self::STATUS_REPLY) {
			return $this->reply;
		}
		return null;
	}
}


/**
 * Exception classes.
 * These extend the PHP Exception class to allow for catching exceptions with 
 * more granularity.
 */
class Txtstream_Exception extends Exception {
}

/**
 * Thrown if invalid parameters are sent to a method.
 */
class Txtstream_ParameterException extends Exception {
}

/**
 * Exception class for errors in network communication, either because invalid 
 * data came back or a transport error occurred.
 */
class Txtstream_NetworkException extends Txtstream_Exception {
}
