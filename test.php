<?php

require_once('TxtstreamAPI.php');

$ts = new TxtstreamAPI('20023', 'minedu12', array());
if ($argv[1]) {
	$msgID = $argv[1];
} else {
	$msgID = null;
	$result = $ts->sendSMS('0212106318', 'Another test message', 'Luke Hudson', 'lukeletters@gmail.com', $msgID);
	print_r($result);
	if ($result instanceof Txtstream_Response) {
		$msgID = $result->messageID;
	}
	sleep(6 * 60 + 1); // Wait 6:01 mins
}
$result = $ts->getMessageStatus($msgID);
print_r($result);
