<?php

App::uses('TxtstreamAPI', 'Vendor/Txtstream');

/**
 * Mock-up class used for testing SMS, where we can avoid sending requests to real API
 */
class MockTxtstreamAPI extends TxtstreamAPI {
		protected static $response = null;
		
		public function set_response($resp) {
				self::$response = $resp;
		}
		
		public function set_throw($ex) {
				self::$response = new Txtstream_NetworkException(sprintf("Network error in TxtstreamAPI::request: %s", $ex->getMessage()), 0, $ex);
		}
		
		protected function request($xml) {
				if (self::$response instanceof Exception) {
						throw self::$response;
				} else {
						return self::$response;
				}
		}
}
