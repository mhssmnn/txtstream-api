<?php
return <<<ENDXML
	<?xml version="1.0" encoding="UTF-8" ?> 
	<messagerequest>
		<header>
			<account>{$username}</account> 
			<password>{$password}</password> 
		</header>
		<getstatus>
			<messageid>{$messageID}</messageid> 
		</getstatus>
	</messagerequest>
ENDXML;
?>
