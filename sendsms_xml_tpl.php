<?php
return <<<ENDXML
	<?xml version="1.0" encoding="UTF-8" ?> 
	<messagerequest>
		<header>
			<account>{$username}</account> 
			<password>{$password}</password>
		</header>
		<request>
			<message>
				{$msgIDElt}
				<mobile>{$toNumber}</mobile> 
				<toname>{$toName}</toname> 
				<fromname>{$fromName}</fromname>
				<replyaddress>{$replyTo}</replyaddress> 
				<messagetext>{$message}</messagetext> 
			</message>
		</request>
	</messagerequest>
ENDXML;
